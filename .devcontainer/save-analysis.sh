#!/usr/bin/env bash

# analysis_data=$(/home/rust/.cargo/bin/cargo xcheck --message-format=json-diagnostic-rendered-ansi)
# analysis_data=$(cat test.out)
# wd=$(pwd)
# i=0
# while IFS= read -r line; do
#     [[ $line = WARNING* ]] && continue
#     reason=$(echo "$line" | jq -c -r '.reason')
#     # echo $reason
#     # if [ $reason == "compiler-message" ]; then
#         src_filename=$(echo "$line" | jq -c -r '.target.src_path')
#         filename="$wd/.rls/analysis/$i.json"
#         # echo "processing file $src_filename to $filename"
#         mkdir -p $(dirname $filename)
#         echo "$line" > $filename
#         echo $filename
#         let i=i+1
#     # fi
# done <<< "$analysis_data"

export RUSTFLAGS="$RUSTFLAGS -Z save-analysis"
silent=$(cargo xbuild)
ls -1d $(pwd)/target/xtensa-esp32-none-elf/debug/deps/save-analysis/*